﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckTheRealTp.ModelsClass
{
    public class Actualite : BindableBase
    {
        private int _idActu;
        public int IdActu
        {
            get { return _idActu; }
            set { SetProperty(ref _idActu, value); }
        }


        private string _titreActu;
        public string TitreActu
        {
            get { return _titreActu; }
            set { SetProperty(ref _titreActu, value); }
        }


        private string _description;
        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }


        private string _urlImage;
        public string UrlImage
        {
            get { return _urlImage; }
            set { SetProperty(ref _urlImage, value); }
        }


        private DateTime _dateDebut;
        public DateTime DateDebut
        {
            get { return _dateDebut; }
            set { SetProperty(ref _dateDebut, value); }
        }


        private DateTime _dateFin;
        public DateTime DateFin
        {
            get { return _dateFin; }
            set { SetProperty(ref _dateFin, value); }
        }


        private string _urlSite;
        public string UrlSite
        {
            get { return _urlSite; }
            set { SetProperty(ref _urlSite, value); }
        }


        private int _idUtilisateur;
        public int IdUtilisateur
        {
            get { return _idUtilisateur; }
            set { SetProperty(ref _idUtilisateur, value); }
        }


        private bool _actif;
        public bool Actif
        {
            get { return _actif; }
            set { SetProperty(ref _actif, value); }
        }


        public Actualite()
        {

        }
        public Actualite(int idActu, string titreActu, string description, string urlImage, DateTime dateDebut, DateTime dateFin, string urlSite, int idUtilisateur, bool actif)
        {
            this.IdActu = idActu;
            this.TitreActu = titreActu;
            this.Description = description;
            this.UrlImage = urlImage;
            this.DateDebut = dateDebut;
            this.DateFin = dateFin;
            this.UrlSite = urlSite;
            this.IdUtilisateur = idUtilisateur;
            this.Actif = actif;
        }
    }
}

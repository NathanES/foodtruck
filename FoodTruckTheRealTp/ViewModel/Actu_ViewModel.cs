﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoodTruckTheRealTp.DAL;
using Prism.Mvvm;
using FoodTruckTheRealTp.ModelsClass;

namespace FoodTruckTheRealTp.ViewModel
{

    public class Actu_ViewModel : BindableBase
    {
        public Actu_ViewModel()
        {
            AfficheActu();
        }
        //numero de l'actu

        public bool passage1 = true;

        private ActualiteDAL _actuTest;
        public ActualiteDAL ActuTest
        {
            get { return _actuTest; }
            set { SetProperty(ref _actuTest, value); }
        }
        private Actualite _actuTest2;
        public Actualite ActuTest2
        {
            get { return _actuTest2; }
            set { SetProperty(ref _actuTest2, value); }
        }

        public async void AfficheActu()
        { 
           int numActu = 1;

            ActuTest = new ActualiteDAL();
            ActuTest2=ActuTest.FindById(numActu);
            if (numActu < 5)
            {
                numActu++;
            }
            else
            {
                numActu = 1;
            }

            await Task.Factory.StartNew(() => {
                System.Threading.Thread.Sleep(3000);                
            });
            numActu++;
        }
    }

}

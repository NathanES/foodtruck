﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoodTruckTheRealTp.ModelsClass;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.Linq;

namespace FoodTruckTheRealTp.DAL
{
    public class ActualiteDAL
    {
        public Actualite FindById(int IdActu)
        {
            Actualite Actu1 = new Actualite();

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["StringName"].ConnectionString);
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "SELECT * FROM ACTUALITES WHERE ID_ACTUALITE = @IDActu";
            cmd.Parameters.AddWithValue("@IDActu", IdActu);

            cmd.Connection = con;
            con.Open();

            SqlDataReader reader = cmd.ExecuteReader(System.Data.CommandBehavior.SingleRow);
            while(reader.Read())
            {
                Actu1.IdActu = reader.GetInt32(0);
                Actu1.TitreActu = reader.GetString(1);
                Actu1.Description = reader.GetString(2);
                Actu1.UrlImage = reader.GetString(3);
                Actu1.DateDebut = reader.GetDateTime(4);
                Actu1.DateFin = reader.GetDateTime(5);
                Actu1.UrlSite = reader.GetString(6);
                Actu1.IdUtilisateur = reader.GetInt32(7);
                Actu1.Actif = reader.GetBoolean(8);
            }

            return Actu1;
        }
    }
}
